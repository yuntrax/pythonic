def translate(sobonbg):
    song = ""
    section = ""
    idx = 0
    while idx < len(sobonbg):
        char = sobonbg[idx]
        if char in (" ", "\n", ",", "."):                 # כל סימן פיסוק אמור ל״קטוע״ את הספירה שלנו. נכניס את הקטע כמו שהוא ונאפס את הקטע.
            song += section + char
            section = ""
            idx += 1
        elif len(section) > 0 and char == "ב": 
            song += section
            idx += len(section)                           # פה קורה הקסם. אם הגענו לאות בית, אנחנו רוצים לבדוק כמה אותיות צברנו עד אליה, ואז ״לקפוץ״ קדימה את אותו מספר אותיות.
            section = ""
        else:
            section += char
            idx += 1
    return song + section                                 # חשוב לזכור, אחרת נאבד את החלק האחרון

# test
test_sobonbg = """אבניבי אובוהבבב,
אבניבי אובוהבבב אובותבך"""
song = translate(test_sobonbg)
print(song)
