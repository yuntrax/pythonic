RUDE_USERS_FILE_LOCATION = "rudes_counter.txt"                               #  מאוד מומלץ לשם קבועים שיחזרו מספר פעמים בקוד בתור קונסטנס
USERS_FILE_LOCATION = "users.txt"                                            #  ככה, אם מישהו ירצה לשנות את ההגדרה הזאת הוא יוכל לעשות את זה בקלות, בלי לקרוא את כל הקוד.
USERS_FILE_DELIMITER = ":"

REGISTRATION_COMPLAIN = "why do i even need to register?"
COMPLAIN_TIMES_COMPLAIN = "why do i need to complain so many times?"


def create_unknown_user():
    with open(RUDE_USERS_FILE_LOCATION) as f:
        counter = int(f.read())
    counter += 1

    with open(RUDE_USERS_FILE_LOCATION, "w") as f:
        f.write(str(counter))
    return f"rude_user_{counter}"


def get_users_data(usernames_only):                                         # בכך שאני מקבל את המשתנה הזה, אני חוסך לעצמי לכתוב פונקציה כמעט זהה. זה מקובל רק אם הפונקציה עדיין מובנת והמטרה לא משנתה. היא עדיין ״מביאה את היוזרים״
    with open(USERS_FILE_LOCATION, "r") as f:
        users = f.readlines()

    idx = 0
    while idx < len(users):
        user_data = users[idx].strip().split(USERS_FILE_DELIMITER)          #  שימו לב לשרשור הפקודות. זה מאוד מקובל כדי להמנע מתוצאות ביינים לא נחוצות. אבל מצד שני צריך לא להגזים עם זה. הכל בגבול הטעם הטוב
        if usernames_only:
            users[idx] = user_data[0]
        else:
            users[idx] = [user_data[0], int(user_data[1])]                  #  שימו לב להמרה לסוג מספר כאן. כשקוראים מתוך קובץ מקבלים מחרוזת, וכדאי לטפל בזה מוקדם
        idx += 1
    return users


def add_user(username):
    if username in get_users_data(usernames_only=True):
        return False

    with open(USERS_FILE_LOCATION, "a") as f:
        f.write(f"{username}{USERS_FILE_DELIMITER}0\n")
    return True


def register(username):
    if username.lower() == REGISTRATION_COMPLAIN:                          # שימו לב שאנחנו עוזרים לפייתון להבין שלא אכפת לנו מאותיות גדולות או קטנות
        username = create_unknown_user()
        print(f"Seriously!? ok your username will be {username}")

    while not add_user(username):                                          # החיבור הזה של הלולאה עם שם פונקציה אינדקטיבי מייצר קוד קריא. ממש כמו סיפור.
        username = input(f"The user '{username}' is already registered. Can you be more creative this time or what? ")

    print(f"You are late, {username}, come in!")


def add_complain_to_user(username, is_cheat):
    users_data = get_users_data(usernames_only=False)
    lines = []
    idx = 0
    while idx < len(users_data):
        current_user = users_data[idx]
        if username == current_user[0]:
            if is_cheat:
                current_user[1] += 10
            else:
                current_user[1] += 1
        lines.append(f"{current_user[0]}{USERS_FILE_DELIMITER}{current_user[1]}\n")
        idx += 1

    with open(USERS_FILE_LOCATION, "w") as f:
        f.writelines(lines)


def complain(username, what_now):
    if username not in get_users_data(usernames_only=True):
        print("Who are you?! what are you doing here???")
        return    
    print("What did you just say? After all I did for you?")
    add_complain_to_user(username, what_now == COMPLAIN_TIMES_COMPLAIN)       # מה שעובר כאן אל הפוקנציה היא התוצאה של התנאי הלוגי. התוצאה הזאת בעצם אומרת האם המשתמש ביקש לרמות או לא.


def enough_is_enough():
    users_data = get_users_data(usernames_only=False)
    idx = 0
    print("Congratulations to:")
    while idx < len(users_data):
        if users_data[idx][1] >= 10:
            print(users_data[idx][0])
        idx += 1
    print("Now go away and give me some peace")


def reset():
    with open(USERS_FILE_LOCATION, "w") as f:
        f.close()                                                             # זה מיצר קובץ ריק בלי לכתוב אליו כלום
    with open(RUDE_USERS_FILE_LOCATION, "w") as f:
        f.write("0")

# test
reset()
register("yaron")
register("Why do I even need to register?")
register("Why do I even need to register?")
complain("yaron", "why?")
complain("yaron", "until when?")
complain("rude_user_2", "why do I need to complain so many times?")
enough_is_enough()
